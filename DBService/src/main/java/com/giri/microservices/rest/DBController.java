package com.giri.microservices.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.giri.microservices.model.DBDetails;
import com.giri.microservices.service.DBService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@RestController
public class DBController {

	@Autowired
	DBService dbService;
	
	@Value("${server.port}")
	private int port;
	
	@GetMapping("/bank-db/environment")
	@HystrixCommand(fallbackMethod="fallbackDeafultDBDetails")
	public ResponseEntity<DBDetails> getDBDetails(){
		
		DBDetails dBDetails = dbService.getDBConnection();
		
		/*
		 * if(dBDetails.getUrl() == null) { throw new
		 * RuntimeException("Failed to load the properties from Remote Configuration Server"
		 * ); }
		 */
		
		System.out.println("Database Environment deatils : " + dBDetails.getUrl());
		
		System.out.println(">>>>>>>>>>>>>dBDetails>>>>>>>>>>>>>"+dBDetails);
		
		return ResponseEntity.ok(dBDetails);				
	}
	
	public ResponseEntity<DBDetails> fallbackDeafultDBDetails(){
		
		DBDetails dBDetails = new DBDetails("deafult url", "default username", "default password", "deafult", port);
		
		return ResponseEntity.ok(dBDetails);				
	}
}
