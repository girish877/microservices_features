package com.giri.microservices.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.giri.microservices.config.Configuration;
import com.giri.microservices.model.DBDetails;

@Service
public class DBService {

	@Autowired
	Configuration configuration;
	
	@Value("${server.port}")
	private int port;
	
	public DBDetails getDBConnection() {
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+port);
		return new DBDetails(configuration.getUrl(), configuration.getUsername(), configuration.getPassword(), configuration.getType(), port);
	}
	
}
