package com.giri.microservices.UserService.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "user-service")
public class Configuration {

	private String isAuth;

	public String getIsAuth() {
		return isAuth;
	}

	public void setIsAuth(String isAuth) {
		this.isAuth = isAuth;
	}

	@Override
	public String toString() {
		return "Configuration [isAuth=" + isAuth + "]";
	}

}