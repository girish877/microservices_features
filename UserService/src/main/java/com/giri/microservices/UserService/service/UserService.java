package com.giri.microservices.UserService.service;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.giri.microservices.UserService.config.Configuration;
import com.giri.microservices.UserService.model.DBDetails;
import com.giri.microservices.UserService.model.User;

@Service
public class UserService {

	@Autowired
	private Configuration configuration;
	
	@Autowired
	private DBServiceProxy dbServiceProxy;
	
	private Map<String, User> users = new HashMap<>();

	@PostConstruct
	public void mockUserData() {
		User user1 = new User("span", "evry");
		User user2 = new User("giri", "gireesh");

		users.put(user1.getUsername(), user1);
		users.put(user2.getUsername(), user2);
	}

	public DBDetails validateUser(String username) {
		DBDetails dbDetails = new DBDetails();
		if (configuration.getIsAuth().equalsIgnoreCase("true")) {
			if (users.containsKey(username)) {
				/*
				 * ResponseEntity<DBDetails> responseEntity = new
				 * RestTemplate().getForEntity("http://localhost:8686//bank-db/environment",
				 * DBDetails.class); dbDetails = responseEntity.getBody();
				 */
				
				dbDetails = dbServiceProxy.getUser();
				
				dbDetails.setIsAuth(configuration.getIsAuth());
				//dbDetails.setPort(port);
			} else {
				System.out.println("Validation is failed........");
			}
			
		} else {
			System.out.println("not validation is requierd.. to enable validation update the proeprties");
		}
		return dbDetails;
	}

	public void registerUser(User user) {

		if (users.containsKey(user.getUsername())) {
			System.out.println("User already exisit...");
			return;
		}

		users.put(user.getUsername(), user);

	}

	public Map<String, User> getUsers(User user) {

		return users;

	}

	public User getUser(String username) {
		System.out.println("----------------------------");
		System.out.println(configuration.getIsAuth());
		System.out.println("----------------------------");
		return users.get(username);
	}

}
