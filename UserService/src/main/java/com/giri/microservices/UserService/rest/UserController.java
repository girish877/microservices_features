package com.giri.microservices.UserService.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.giri.microservices.UserService.model.DBDetails;
import com.giri.microservices.UserService.service.UserService;

@RestController
public class UserController {
	
	@Autowired
	UserService userService;
	
	
	@GetMapping("/user-service/{username}")
	public ResponseEntity<DBDetails> validateUsername(@PathVariable("username") String username){
		
		DBDetails dbDetails = userService.validateUser(username);
		
		System.out.println("User Validation Status : " + dbDetails);
		
		System.out.println("+++++++++dbDetails+++++++"+dbDetails);
		
		return ResponseEntity.ok(dbDetails);				
	}

}
