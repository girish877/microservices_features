package com.giri.microservices.UserService.model;

public class DBDetails {

	private String url;
	private String username;
	private String password;
	private String type;

	private int port;
	private String isAuth;

	public DBDetails() {
		super();
	}

	public DBDetails(String url, String username, String password, String type) {
		super();
		this.url = url;
		this.username = username;
		this.password = password;
		this.type = type;
	}

	public DBDetails(String url, String username, String password, String type, int port) {
		super();
		this.url = url;
		this.username = username;
		this.password = password;
		this.type = type;
		this.port = port;
	}
	
	public DBDetails(String url, String username, String password, String type, int port, String isAuth) {
		super();
		this.url = url;
		this.username = username;
		this.password = password;
		this.type = type;
		this.port = port;
		this.isAuth = isAuth;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getIsAuth() {
		return isAuth;
	}

	public void setIsAuth(String isAuth) {
		this.isAuth = isAuth;
	}

	@Override
	public String toString() {
		return "DBDetails [url=" + url + ", username=" + username + ", password=" + password + ", type=" + type
				+ ", port=" + port + ", isAuth=" + isAuth + "]";
	}

}
