package com.giri.microservices.UserService.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import com.giri.microservices.UserService.model.DBDetails;


//@FeignClient(name="bank-db", url="localhost:8686") // - one more we can configure by using ribbon
@FeignClient(name="ZULU-SERVER")
public interface DBServiceProxy {
	
	@GetMapping("/bank-db/bank-db/environment")
	public DBDetails getUser();
}
